DIR=$(shell pwd)
YEAR ?= 2016-10

# Preparation procedures

# downloads dbpedia core, ontology and en folders
# https://wiki.dbpedia.org/public-sparql-endpoint
download:
	wget -r -nc -nH --cut-dirs=1 -np -l1 \
		-A '*.ttl.bz2' -A '.nt.bz2' -R '*unredirected*' \
		-P ./dbpedia http://downloads.dbpedia.org/$(YEAR)/links/
	wget -r -nc -nH --cut-dirs=1 -np -l1 \
		-A '*.ttl.bz2' -R '*unredirected*' \
		-P ./dbpedia http://downloads.dbpedia.org/$(YEAR)/core/
	wget -nc -P ./dbpedia/core-i18n http://downloads.dbpedia.org/$(YEAR)/core-i18n/en/instance_types_lhd_dbo_en.ttl.bz2
	wget -nc -P ./dbpedia/core-i18n http://downloads.dbpedia.org/$(YEAR)/core-i18n/en/instance_types_lhd_ext_en.ttl.bz2
	wget -nc -P ./dbpedia/classes http://downloads.dbpedia.org/$(YEAR)/dbpedia_$(YEAR).owl
	rm ./dbpedia/robots.txt.tmp

# unpacks downloaded files
unpack:
	for i in ./dbpedia/links/*.bz2 ; do echo $$i ; bzip2 -d "$$i"; done
	for i in ./dbpedia/core/*.bz2 ; do echo $$i ; bzip2 -d "$$i"; done
	for i in ./dbpedia/core-i18n/*.bz2 ; do echo $$i ; bzip2 -d "$$i"; done

# moves unpacked files into separate folders for import
move:
	mkdir ./dbpedia/links-ttl
	mv ./dbpedia/links/* ./dbpedia/links-ttl/
	mkdir ./dbpedia/core-ttl
	mv ./dbpedia/core/*.ttl ./dbpedia/core-ttl/
	mkdir ./dbpedia/core-i18n-ttl
	mv ./dbpedia/core-i18n/*.ttl ./dbpedia/core-i18n-ttl/

# executes download, unpack and move one after another
prepare: download unpack move

# creates new instance of virtuoso
virtuoso-docker:
	docker run --name dbpedia \
	    -p 8890:8890 -p 1111:1111 \
		-v ${DIR}/db:/data \
		-v ${DIR}/virtuoso/virtuoso.ini:/data/virtuoso.ini \
		-v ${DIR}/dbpedia:/import \
		-d tenforce/virtuoso

# stops and removes virtuoso
stop-virtuoso:
	docker stop dbpedia && docker rm dbpedia

# starts importing the data into running instance of virtuoso
import:
	docker run --name dbpedia-import \
		-it \
		--rm \
		--link dbpedia \
		-v ${DIR}/dbpedia:/import \
		-v ${DIR}/virtuoso/import.sh:/import/import.sh \
		tenforce/virtuoso bash /import/import.sh
